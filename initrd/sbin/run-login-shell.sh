#!/bin/sh
# This is a helper script to make agetty call *login* shell without asking
# username and password. This is necessary because agetty (at least current
# version) doesn't understand parameters in login command.

exec /bin/login -f root

