#!/bin/sh
# This is a helper script to make agetty call frontend program if *login*
# shell without asking username and password. If /frontend/runmain.sh
# exists it is run, otherwise, login shell is invoked. This is necessary
# because agetty (at least current version) doesn't understand parameters
# in login command.

[ -x /frontend/runmain.sh ] \
	&& /bin/sh -l /frontend/runmain.sh \
	|| exec /bin/login -f root
