#!/usr/bin/perl
# This script generates cpio archive, which can be used as ramdisk image for
# diskless Linux boot. The script doesn't use mknod so it doesn't require root
# privileges. In fact, it can be used even under Windows (with Cygwin) without
# any Linux installation at all.
#
#     make-cpio.pl BASE_DIR <CONFIG_FILE >CPIO_FILE
#
# See corresponding documentation for further details.
#
# Copyright (c) 2011, Pavel Kretov <kretovpa@gmail.com>.
#
# This code is distributed under the terms of GNU General Public License (GPL)
# as it is based on Juan Jose Pablos' mknod-cpio.pl script from unattended
# project (http://unattended.sourceforge.net).

use warnings;
use strict;
use bytes;

sub command_set(@);
sub cpio_header($);
sub cpio_file($$);
sub cpio_dir($);
sub cpio_link($$);
sub cpio_node($$$$);
sub cpio_trailer();

sub uniq_inode();
sub parse_command($\$\@\%);
sub parse_variables($);
sub variable($);
sub unslash($);
sub fullpath($);

our $g_currentInode = 1;
our $g_basedir = ".";
our %g_inlineVariables;
our %g_variables = (
    "mode" => 644,
    "uid"  => 0,
    "gid"  => 0,
);

# The list of already created directories.
our %g_createdDirectories;

# --- main ---

binmode STDOUT, ":raw";

if ($#ARGV >= 0) {
    $g_basedir = $ARGV[0];
}

while (my $line = <STDIN>) {
    my ($cmd, @args, %vars);
    my $status = parse_command($line, $cmd, @args, %vars);

    ($status ne "comment") or
        next;

    ($status ne "error") or
        die "syntax error: unable to parse input line";

    if (%vars) { %g_inlineVariables = %vars; }
    else { undef %g_inlineVariables; }

    # Make command name case-insensitive.
    $cmd = lc $cmd;

    my %func = (
        "set"  => \&command_set,
        "file" => \&cpio_file,
        "dir"  => \&cpio_dir,
        "link" => \&cpio_link,
        "node" => \&cpio_node);

    $func{$cmd} or die "syntax error: unrecognized command";
    $func{$cmd}->(@args);
}

cpio_trailer();
exit 0;

# --- functions ---

# Parses given line to command name, list of parameters
# and hash of local variables.
sub parse_command($\$\@\%) {
    my ($line, $cmdref, $argsref, $varsref) = @_;

    $line =~ s/\#.*//;  #< remove comments
    $line =~ s/^\s+//;  #< remove leading spaces
    $line =~ s/\s+$//;  #< remove trailing spaces

    if ($line =~ /^\s*$/) { return "comment"; }

    my ($cmd, @args, %vars);
    ($cmd, @args) = split /\s+/, $line;

    my $lastarg = $args[$#args];
    if ($lastarg =~ /^\[.+\]$/) {
        $lastarg = substr $lastarg, 1, length($lastarg)-2;
        %vars = parse_variables($lastarg);

        if (%vars) {
            %$varsref = %vars;
            pop @args;
        }
    }

    $$cmdref  = $cmd;
    @$argsref = @args;
    %$varsref = %vars;
    return "okay";
}

# Returns a hash from a list of comma separated
# list of key-value pairs given as the argument.
sub parse_variables($) {
    my $s = shift;
    return undef if $s !~ m/^((\w+)=([\w\d]+),?)+$/;

    my %hash;
    foreach my $keyval (split ",", $s) {
        my ($key, $val) = split "=", $keyval;
        $hash{lc($key)} = $val;
    }

    return %hash;
}

# Returns a value of variable, whose name is given as the argument.
# Subroutine searches variables at first in %g_inlineVariables,
# then in %g_variables.
sub variable($) {
    my $name = shift;
    return $g_inlineVariables{$name} if defined $g_inlineVariables{$name};
    return $g_variables{$name} if defined $g_variables{$name};
    return undef;
}

# Removes leading slash from file path.
sub unslash($) {
    my $name = shift;
    $name =~ s/^\/+//;
    return $name;
}

# Returns full path to file given as the argument
# relatively to base directory.
sub fullpath($) {
    my $name = shift;
    return $g_basedir."/".unslash($name);
}

# Extracts directory part of file path by removing everything after 
# the last slash. If there is no slashes in file path, empty string
# is returned.
sub dirname($) {
    my $filename = shift;
    $filename =~ s/\/[^\/]*$//g;
    return $filename;
}

# Generates unique i-node number. This i-node is internal for archive
# and don't have to correlate with real file i-nodes.
sub uniq_inode() {
    return $g_currentInode++;
}

# Return enough null characters to align
# argument size to a multiple of 4 bytes.
sub align ($) {
    my $rest = (length shift) % 4;
    return (!$rest) ? "" : "\x00"x(4-$rest);
}

# Implementation of "set" command. Applies new values to variables.
sub command_set(@) {
    my %vars = parse_variables(join(",", @_));
    %vars or
        die "syntax error: invalid variables list";

    # Here we must add modified values to %g_variables hash.
    foreach my $key (keys %vars) {
        $g_variables{$key} = $vars{$key};
    }

    # This command doesn't produce any output.
    return "";
}

# Creates all subdirectories unless they are already created.
sub force_directories($) {
    my $dirname = unslash(shift);
    my @dirs = split "/", $dirname;

    # HACK: Modify mode during directories creation.
    my $oldmode = $g_inlineVariables{"mode"};
    $g_inlineVariables{"mode"} = variable("dirmode");

    my $new = 0;
    my $path = "";
    foreach my $dir (@dirs) {
        $path .= "/$dir";
        if (!$g_createdDirectories{$path}) {
            cpio_dir($path);
            $g_createdDirectories{$path} = 1;
            $new = 1;
        }
    }

    $g_inlineVariables{"mode"} = $oldmode;
    return $new;
}

# Generates CPIO entry header by filling header fields with given
# values. All that values (except magic) must be integer numbers.
sub cpio_header($) {
    my %actual = %{shift @_};
    my @fields = (
        magic     => "070701",
        ino       => 0,
        mode      => 0,
        uid       => 0,
        gid       => 0,
        nlink     => 0,
        mtime     => 0,
        filesize  => 0,
        devmajor  => 0,
        devminor  => 0,
        rdevmajor => 0,
        rdevminor => 0,
        namesize  => 0,
        check     => 0,
    );

    my %default = @fields;

    # Helper sub to get rid of monstrous ?: constructs. Do *not* rewrite
    # it as a nested named subroutine, despite it would look better.
    my $param = sub {
        return $actual{$_[0]} ? $actual{$_[0]} : $default{$_[0]};
    };

    my $result = sprintf "%06s", $param->("magic");
    length $result <= 6 or
        die "internal error: header magic doesn't fit in 6 characters";

    my $index = -1; # <- NB!
    foreach my $field (@fields) {
        $index++;

        # Skip magic and all odd indexes in @fields array.
        if ($index==0 || $index % 2) { next; }

        my $hexacat = sprintf "%08X", $param->($field);
        length $hexacat <= 8 or
            die "internal error: header field doesn't fit in 8 characters";

        $result .= $hexacat;
    }

    return $result;
}

# Produce CPIO entry for regular file, including its data.
sub cpio_file($$) {
    my ($name, $srcfilename) = @_;
    defined $name
        or die "syntax error: too few arguments for \"file\" command";

    force_directories(dirname($name));
    print STDERR " >> creating file: $name\n";

    # If the second argument is not specified, use the first one
    # as source file name.
    $srcfilename = unslash($name) unless $srcfilename;
    $srcfilename = fullpath($srcfilename);

    my $content;
    my $name0 = unslash($name)."\x00";
    my $mode  = (8 << 12) | oct(variable("mode"));

    # NB: raw mode.
    open FILE, "<:raw", $srcfilename or
        die "error: unable to open file \"$srcfilename\"";

    # Read the whole file content to variable.
    while (<FILE>) {
        $content .= $_;
    }

    close FILE;

    my $result = cpio_header {
        ino      => uniq_inode(),
        mode     => $mode,
        uid      => variable("uid"),
        gid      => variable("gid"),
        nlink    => 1,
        mtime    => time(),
        filesize => length($content),
        namesize => length($name0),
    };

    $result .= $name0;
    $result .= align($result);
    $result .= $content;
    $result .= align($result);
    print $result;
}

sub cpio_dir($) {
    my $name = shift;
    defined $name or
        die "syntax error: too few arguments for \"dir\" command";

    print STDERR " >> creating dir:  $name\n";
    my $name0 = unslash($name)."\x00";
    my $mode = (4 << 12) | oct(variable("mode"));

    my $result = cpio_header {
        ino       => uniq_inode(),
        mode      => $mode,
        uid       => variable("uid"),
        gid       => variable("gid"),
        nlink     => 1,
        mtime     => time,
        namesize  => length $name0,
    };

    $result .= $name0;
    $result .= align($result);
    print $result;
}

# Produce CPIO entry for a symbolic link.
sub cpio_link($$) {
    my ($name, $content) = @_;
    defined $name &&
    defined $content or
        die "syntax error: too few arguments for \"link\" command";

    force_directories(dirname($name));
    print STDERR " >> creating link: $name -> $content\n";

    my $name0 = unslash($name)."\x00";
    my $mode  = (10 << 12) | oct(variable("mode"));

    my $result = cpio_header {
        ino      => uniq_inode(),
        mode     => $mode,
        uid      => variable("uid"),
        gid      => variable("gid"),
        nlink    => 1,
        mtime    => time(),
        filesize => length $content,
        namesize => length $name0,
    };

    $result .= $name0;
    $result .= align($result);
    $result .= $content;
    $result .= align($result);
    print $result;
}

# Produce CPIO entry for a device node.
sub cpio_node($$$$) {
    my ($name, $type, $major, $minor) = @_;

    defined $name &&
    defined $type &&
    defined $major &&
    defined $minor or
       die "syntax error: too few arguments for \"node\" command";

    force_directories(dirname($name));
    print STDERR " >> creating node: $name ($type: $major, $minor)\n";

    my $name0 = unslash($name)."\x00";
    my $mode = 0;

    $mode = 2 << 12 if $type eq "c";
    $mode = 6 << 12 if $type eq "b";
    $mode or
        die "syntax error: wrong device type specified";

    $mode |= oct(variable("mode"));

    my $result = cpio_header {
        ino       => uniq_inode(),
        mode      => $mode,
        uid       => variable("uid"),
        gid       => variable("gid"),
        nlink     => 1,
        mtime     => time,
        rdevmajor => $major,
        rdevminor => $minor,
        namesize  => length $name0,
    };

    $result .= $name0;
    $result .= align($result);
    print $result;
}

# Produce CPIO trailer entry.
sub cpio_trailer () {
    my $trailer0 = "TRAILER!!!\x00";
    my $result = cpio_header {
         ino      => uniq_inode(),
         namesize => length $trailer0,
    };

    $result .= $trailer0;
    $result .= align($result);
    print $result;
}

