@echo off
set SYSLINUX_ROOT=syslinux
set SYSLINUX_OPTS=--active --mbr

title Make bootable flash drive
echo Auxiliary script for making bootable USB flash drives.
echo:
echo This script will install syslinux bootloader on your flash drive. Please
echo make sure you have started this script from a FLASH DRIVE, not your hard
echo drive because it may damage you operating system bootloader. Please note
echo that you must run this script as Administrator.
echo:

cd \
echo Current drive is detected as %CD%, if it is correct, press Return,
echo otherwise close this window.
echo:

set /p BUF= Is everything okay?
echo:

%SYSLINUX_ROOT%\syslinux.exe %SYSLINUX_OPTS% --directory /%SYSLINUX_ROOT% --install %CD:~0,-1%
if errorlevel 1 (
echo Something went WRONG! This may happen if you don't have enough privilege
echo in system or if drive is write-protected. Or maybe you just missed drive
echo letter and the system is unable to find syslinux.exe program.
) else (
echo Bootloader seems to be SUCCESSFULLY INSTALLED.
)

echo:
set /p BUF= Press Return to exit
