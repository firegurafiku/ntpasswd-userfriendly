ntpasswd-userfriendly
=====================

Overview
--------
This project is aimed to be a user-friendly reimplementation of ntpasswd boot
disk. It replaces shell-based dialogs with more convenient user interface
with arrow keys selection. There also planned internationalization of some
kind.

Thirdparty software
-------------------
The ntpasswd-uf live system is based on Linux kernel and uclibc library. But it
also contains many other programs, all linked against uclibc. Every piece of
code was build using buildroot framework. In the list below you can see every
thirdparty program used in this project alongside with its version and license:

    # build framework:
    buildroot               2011.08     ?
    
    # user-friendly scripts:
    ntpasswd-uf-scripts     ?           ?
    
    # thirdparty:
    agetty                  ?           ?
    blkid                   ?           ?
    busybox                 ?           ?
    bzip2                   ?           ?
    cfdisk                  ?           ?
    fdisk                   ?           ?
    file                    ?           ?
    hdparm                  ?           ?
    ldconfig                ?           ?
    ldd                     ?           ?
    linux kernel            2.6.39.3    GNU GPL v2
    nano                    ?           ?
    ntfs-3g                 ?           ?
    partimage               ?           ?
    sdparm                  ?           ?
    setsid                  ?           ?
    setterm                 ?           ?
    uclibc                  ?           ?
    udev                    ?           ?
    whereis                 ?           ?
    which                   ?           ?

