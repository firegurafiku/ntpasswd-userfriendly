# Makefile: script for building live system images.
#
# This is a make scenario for GNU make program. Note that make is notorious
# for its file format which threats tab character differently from other white
# spaces. Be careful! All shell commands in targets definitions *must* start
# with a single tab character.
#
# Copyright (c) Voronezhselmash, Voronezh, Russia, 2011.
# Copyright (c) Voronezh State University, Russia, 2011.

# --- parameters ---
BASENAME                := ntpasswd-userfriendly
VERSION                 := 2011.08

# --- internals ---
TOPDIR                  := $(shell pwd)
BUILD_DIR               := ${TOPDIR}/build
INITRD_BASE_DIR         := ${TOPDIR}/initrd
INITRD_CONFIG_FILE      := ${TOPDIR}/config/initrd.conf
INITRD_IMAGE_FILE       := ${BUILD_DIR}/initrd.cpio
INITRD_IMAGE_GZ_FILE    := ${INITRD_IMAGE_FILE}.gz
INITRD_FILE_NAME        := rootfs
FRONTEND_BASE_DIR       := ${TOPDIR}/frontend
FRONTEND_CONFIG_FILE    := ${TOPDIR}/config/frontend.conf
FRONTEND_IMAGE_FILE     := ${BUILD_DIR}/frontend.cpio
FRONTEND_IMAGE_GZ_FILE  := ${FRONTEND_IMAGE_FILE}.gz
FRONTEND_FILE_NAME      := frontend
ISO_BASE_DIR            := ${TOPDIR}/disk
ISO_IMAGE_NAME          := ${BASENAME}-${VERSION}.iso
ISO_IMAGE_FILE          := ${TOPDIR}/${ISO_IMAGE_NAME}
ISOLINUX_DIR_NAME       := syslinux
ISOLINUX_BOOT_FILE_NAME := isolinux.bin
ISOLINUX_CAT_FILE_NAME  := isolinux.cat
MAKE_CPIO_SCRIPT        := ${TOPDIR}/scripts/make-cpio.pl

# --- implementation ---
.PHONY: iso initrd frontend help

all: iso

help:
	@ echo 'Live system build script.'
	@ echo 'Copyright (c) Voronezhselmash, Voronezh, Russia, 2011.'
	@ echo 'Copyright (c) Voronezh State University, Russia, 2011.'
	@ echo
	@ echo 'Usage: make [TARGETS]'
	@ echo
	@ echo 'Targets available:'
	@ echo
	@ echo '    help       Show this help message.'
	@ echo '    initrd     Create initrd archive.'
	@ echo '    frontend   Create frontend archive.'
	@ echo '    iso        Create ISO image.'
	@ echo

frontend:
	@ echo "*** building frontend (build/frontend.cpio.gz)..."
	@ [ -d "${BUILD_DIR}" ] || mkdir -p "${BUILD_DIR}"
	@ perl "${MAKE_CPIO_SCRIPT}" "${FRONTEND_BASE_DIR}" \
		<"${FRONTEND_CONFIG_FILE}" \
		>"${FRONTEND_IMAGE_FILE}"
	@ gzip -f "${FRONTEND_IMAGE_FILE}"	
	
initrd:
	@ echo "*** building initrd (build/initrd.cpio.gz)..."
	@ [ -d "${BUILD_DIR}" ] || mkdir -p "${BUILD_DIR}"
	@ perl "${MAKE_CPIO_SCRIPT}" "${INITRD_BASE_DIR}" \
		<"${INITRD_CONFIG_FILE}" \
		>"${INITRD_IMAGE_FILE}"
	@ gzip -f "${INITRD_IMAGE_FILE}"

iso: initrd frontend
	@ echo -e "*** creating cdrom image (${ISO_IMAGE_NAME})..."
	@ mkisofs -verbose \
		-graft-points \
		-no-emul-boot \
		-boot-info-table \
		-boot-load-size 4 \
		-hide-rr-moved \
		-R -J \
		-b "${ISOLINUX_DIR_NAME}/${ISOLINUX_BOOT_FILE_NAME}" \
		-c "${ISOLINUX_DIR_NAME}/${ISOLINUX_CAT_FILE_NAME}" \
		-o "${TOPDIR}/${ISO_IMAGE_NAME}" \
		-m ".svn" \
		"/${ISOLINUX_DIR_NAME}/${INITRD_FILE_NAME}=${INITRD_IMAGE_GZ_FILE}" \
		"/${ISOLINUX_DIR_NAME}/${FRONTEND_FILE_NAME}=${FRONTEND_IMAGE_GZ_FILE}" \
		"/=${ISO_BASE_DIR}"

